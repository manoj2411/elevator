require_relative './elevator'

class Building
  attr_accessor :floors, :elevators

  def initialize(floors, elevators)
    @floors = floors
    @elevators = []
    build_elevators(elevators)
  end

  def build_elevators(n, random_floors = true)
    for i in 0...n
      curr_floor = random_floors ? rand(floors) : 0
      curr_direction = curr_floor < 5 ? 'up' : 'down'
      self.elevators << Elevator.new("e#{i}", curr_floor, curr_direction, self)
    end
  end

  def start_elevators
    if !@elevators.is_a?(Array)
      puts 'No elevators in building'
    end
    # @elevators.each {|ele| ele.start }
  end

  def next_tick
    @elevators.each {|ele| ele.next_tick }
  end
end
