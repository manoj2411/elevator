class RequestElevatorMatcher
  attr_reader :request, :elevators

  def initialize(request, elevators)
    @request, @elevators = request, elevators
    @selected_elevator = nil
  end

  def run
    # 1. get same direction above/below
    same_direction_elevator = get_same_direction_elevator
    # 2. waiting nearest
    ideal_elevator = get_ideal_elevator
    # 3. assign the closest
    if same_direction_elevator == nil
      @selected_elevator = ideal_elevator
    elsif ideal_elevator == nil
      @selected_elevator = same_direction_elevator
    end
    self
  end

  def matched?
    elevator != nil
  end

  def elevator
    @selected_elevator
  end

  private
    def get_ideal_elevator
      selected = nil
      curr_floor = request[0]
      elevators.each do |elevator|
        if !elevator.running?
          if (selected == nil) ||
            (selected && (selected.curr_floor - curr_floor).abs > (elevator.curr_floor - curr_floor).abs)
            selected = elevator
          end
        end
      end
      return elevator
    end

    def get_same_direction_elevator
      selected = nil
      curr_floor = request[0]

      if request[1] == 'up'
        elevators.each do |elevator|
          if elevator.going_up? && elevator.curr_floor <= curr_floor
            if (selected == nil) || (selected && selected.curr_floor < elevator.curr_floor)
              selected = elevator
            end
          end
        end

      elsif request[1] == 'down'
        elevators.each do |elevator|
          if elevator.going_down? && elevator.curr_floor >= curr_floor
            if (selected == nil) || (selected && selected.curr_floor > elevator.curr_floor)
              selected = elevator
            end
          end
        end
      else
        raise 'Invalid direction requested'
      end
      return selected
    end
end
