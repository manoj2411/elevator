class Elevator
  STOPAGE_TIME = 2
  TICK_TIME = 1
  DEFAULT_WEIGHT = 800

  attr_reader :curr_floor, :curr_direction, :building, :requests,
              :max_weight, :curr_weight, :status, :label, :max_floors,
              :button_panel

  def initialize(label, curr_floor, curr_direction, building, max_weight = DEFAULT_WEIGHT)
    @label = label
    @curr_floor = curr_floor
    @max_weight = max_weight
    @building = building
    @curr_direction = curr_direction
    @max_floors = building.floors
    @requests = Array.new(max_floors, false)
  end

  def stop
    @status = 'stopped'
  end

  def going_down?
    curr_direction == 'down'
  end

  def going_up?
    curr_direction == 'up'
  end

  def stop_at_curr_floor?
    @requests[curr_floor]
  end

  def any_pending_request?
    requests.any?{|req| req}
  end

  def running?
    @status == 'running'
  end

  def start
    unless running?
      @status = 'running'
    end
  end

  def add_request(request)
    puts "#{label} -> #{request[0]}"
    @requests[request[0]] = true
  end

  def next_tick
    # puts "Next tick #{label} #{curr_floor}"
    if any_pending_request? && !running?
      start
    end

    if !any_pending_request?
      puts "#{label} waiting at #{curr_floor}"
      stop
      return
    else
      sleep TICK_TIME
    end

    if going_down? && curr_floor == max_floors - 1
      @curr_direction = 'down'
    elsif curr_floor == 0
      @curr_direction = 'up'
    end

    if stop_at_curr_floor?
      puts "#{label} stopped at #{curr_floor}"
      @requests[curr_floor] = false
      sleep STOPAGE_TIME
    end

    if going_down?
      @curr_floor -= 1
    else
      @curr_floor += 1
    end

  end

end
