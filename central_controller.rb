require_relative './building'
require_relative './request_elevator_matcher'

class CentralController
  attr_accessor :building, :requests

  def initialize(floors, elevators, requests)
    self.building = Building.new(floors, elevators)
    self.requests = requests
  end

  def start_elevators
    building.start_elevators
  end

  def assign_pending_requests
    pending_requests = []
    for i in 0...requests.length
      request = requests[i]
      request_matcher = RequestElevatorMatcher.new(request, building.elevators).run
      if request_matcher.matched?
        request_matcher.elevator.add_request(request)
      else
        pending_requests.push(request)
      end
    end
  end

  def tick
    assign_pending_requests
    building.next_tick
    puts
  end

end
