require_relative './central_controller'

floors = 11
elevators = 3
requests = [[2, 'up'], [6, 'up'], [7, 'down'], [4, 'down']]
controller = CentralController.new(floors, elevators, requests)
controller.start_elevators

5.times { controller.tick }
